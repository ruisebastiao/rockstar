import QtQuick 2.6
import Material 0.3

TabbedPage {
    id: page
    title: "Music"

    actions: [
        Action {
            name: "Options"
            iconSource: "icon://awesome/cog"
        }
    ]

    Tab {
        title: "Player"
        Rectangle { color: Palette.colors.red["200"]}
    }

    Tab {
        title: "Internet Radio"
        Rectangle { color: Palette.colors.purple["200"] }
    }

    Tab {
        title: "Pandora"
        Rectangle { color: Palette.colors.orange["200"] }
    }

    Component.onDestruction: console.log("going away")
}
