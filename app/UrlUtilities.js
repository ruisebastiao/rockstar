
.pragma library

function makeQuery(obj) {
    if ( typeof obj === "Undefined" ) {
        return null;
    }

    if ( obj ) {
        var result = [];
        Object.getOwnPropertyNames(obj).forEach(function(prop) {
            result.push(encodeURIComponent(prop) + "=" + encodeURIComponent(obj[prop]));
        });

        return result.join("&");
    } else {
        return null;
    }
}
