import QtQuick 2.0
import Material 0.3
import Rockstar.DBus 1.0

import "ObjectUtilities.js" as ObjectUtilities

SummaryCard {

    id: weatherCard

    property var __weather: Weather {
        id: currentWeather
        zipcode: "19380,us"
    }

    title: currentWeather.name
    iconSource: currentWeather.iconSource

    Column {
        spacing: dp(10)
        Grid {
            columns: 2
            spacing: dp(5)

            Label {
                text: "Temprature:"
            }
            Label {
                text: "%1 F".arg(currentWeather.temp)
            }

            Label {
                text: "Humidity:"
            }
            Label {
                text: "%1 %".arg(currentWeather.humidity)
            }

            Label {
                text: "Pressure:"
            }
            Label {
                text: "%1 hPa".arg(currentWeather.pressure)
            }
        }
        Label {
            text: currentWeather.text
        }

    }
}
