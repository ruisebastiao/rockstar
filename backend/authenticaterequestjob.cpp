#include "authenticaterequestjob.h"
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QUrl>
#include <QUrlQuery>

AuthenticateRequestJob::AuthenticateRequestJob(QNetworkAccessManager *parent) :
    QObject(parent),
    m_nam(parent)
{

}

void AuthenticateRequestJob::authenticate(const QString &productID,
                                          const QString &clientID,
                                          const QString &clientSecret)
{
    m_productId = productID;
    m_clientId = clientID;
    m_clientSecret = clientSecret;

    QJsonDocument json = QJsonDocument::fromVariant(
                QVariantMap(
    {
                        {"alexa:all", QVariantMap(
                         {
                             {"productID", m_productId},
                             {"productInstanceAttributes" , QVariantMap(
                              {
                                  {"deviceSerialNumber","001"}
                              }
                              )}
                         })
                        }}));

    qDebug() << Q_FUNC_INFO << productID << clientID << clientSecret;


    QUrl requestUrl = QString("https://www.amazon.com/ap/oa");
    QUrlQuery query;
    query.addQueryItem("client_id", m_clientId);
    query.addQueryItem("scope","alexa:all");
    query.addQueryItem("scope_data", json.toJson(QJsonDocument::Compact));
    query.addQueryItem("response_type", "code");
    query.addQueryItem("redirect_uri", "https://localhost/code");
    requestUrl.setQuery(query);

    QNetworkRequest request;
    request.setUrl(requestUrl.url());
    qDebug() << Q_FUNC_INFO << requestUrl.url();

    QNetworkReply *reply = m_nam->get(request);
    connect(reply, SIGNAL(finished()), this, SLOT(extractRedirect()));
}

void AuthenticateRequestJob::extractRedirect()
{
    QUrl redirect;

    QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
    if ( reply ) {
        redirect = reply->header(QNetworkRequest::LocationHeader).toUrl();
        reply->deleteLater();

        qDebug() << Q_FUNC_INFO << redirect;
        emit userPrompt(QUrl(redirect));
        emit finished();
    }

}

void AuthenticateRequestJob::extractCode(const QUrl &redirect, const QString &clientID, const QString &clientSecret )
{

    m_clientId = clientID;
    m_clientSecret = clientSecret;

    qDebug() << Q_FUNC_INFO << redirect;

    QUrlQuery query = QUrlQuery(redirect);
    QString code = query.queryItemValue(QLatin1String("code"), QUrl::FullyDecoded);

    QUrl requestUrl = QString("https://api.amazon.com/auth/o2/token");

    QUrlQuery payload;
    payload.addQueryItem(QLatin1String("client_id"),m_clientId);
    payload.addQueryItem(QLatin1String("client_secret"),m_clientSecret);
    payload.addQueryItem(QLatin1String("code"),code);
    payload.addQueryItem(QLatin1String("grant_type"), QLatin1String("authorization_code"));
    payload.addQueryItem(QLatin1String("redirect_uri"), QLatin1String("https://localhost/code"));

    QNetworkRequest request;
    request.setUrl(requestUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");

    qDebug() << Q_FUNC_INFO << payload.toString(QUrl::FullyEncoded);
    QNetworkReply *postReply = m_nam->post(request, payload.toString(QUrl::FullyEncoded).toUtf8());
    connect( postReply, SIGNAL(finished()), this, SLOT(extractRefreshToken()));
}

void AuthenticateRequestJob::extractRefreshToken()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
    if ( reply ) {
        QByteArray payload = reply->readAll();

        qDebug() << Q_FUNC_INFO << payload;

        QJsonParseError error;
        QJsonDocument response = QJsonDocument::fromJson( payload, &error );


        if ( error.error == QJsonParseError::NoError ) {
            QString token = response.object().value(QLatin1String("refresh_token")).toString();
            emit authToken( token );
        } else {
            qWarning() << Q_FUNC_INFO << error.errorString();
        }
        reply->deleteLater();
    }

    emit finished();

}
