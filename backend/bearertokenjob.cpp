#include "bearertokenjob.h"
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrlQuery>
#include <QUrl>
#include <QJsonDocument>
#include <QJsonParseError>
#include <QJsonObject>

BearerTokenJob::BearerTokenJob(QNetworkAccessManager *parent) : QObject(parent), m_nam(parent)
{

}

void BearerTokenJob::getToken(const QString &clientID, const QString &clientSecret, const AccessToken &token, const QString &refreshToken)
{

    if ( token.expire > QDateTime::currentDateTime() &&
         !token.token.isEmpty() ) {
        emit accessToken(token);
    } else {
        QUrl requestUrl = QString("https://api.amazon.com/auth/o2/token");

        QUrlQuery payload;
        payload.addQueryItem(QLatin1String("client_id"),clientID);
        payload.addQueryItem(QLatin1String("client_secret"),clientSecret);
        payload.addQueryItem(QLatin1String("refresh_token"),refreshToken);
        payload.addQueryItem(QLatin1String("grant_type"), QLatin1String("refresh_token"));
        payload.addQueryItem(QLatin1String("redirect_uri"), QLatin1String("https://localhost/code"));

        QNetworkRequest request;
        request.setUrl(requestUrl);

        QNetworkReply *reply = m_nam->post(request, payload.toString().toUtf8());
        connect( reply, SIGNAL(finished()), this, SLOT(extractToken()));
    }
}

void BearerTokenJob::extractToken()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
    if ( reply ) {

        QJsonParseError error;
        QJsonDocument response = QJsonDocument::fromJson( reply->readAll(), &error );

        if ( error.error == QJsonParseError::NoError ) {
            AccessToken token;
            token.token = response.object().value(QLatin1String("access_token")).toString();
            token.expire = QDateTime::currentDateTime().addSecs(3000);
            emit accessToken(token);
        } else {
            qWarning() << Q_FUNC_INFO << error.errorString();
        }
        reply->deleteLater();
    }
    emit finished();
}
