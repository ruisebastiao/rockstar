#include "dbusutilities.h"
#include <QVariantMap>
#include <QDBusMessage>
#include <QDBusConnection>

DBusUtilities::DBusUtilities()
{

}

void DBusUtilities::emitPropertyChanged(const QString &interface, const QString &path, const QString &name, const QVariant &value)
{
    QVariantMap map;
    QStringList invalid;
    map[name] = value;
    invalid << name;

    QVariantList args;

    args << interface
         << map
         << invalid;

    QDBusMessage sig = QDBusMessage::createSignal(
                path,
                QLatin1String("org.freedesktop.DBus.Properties"),
                QLatin1String("PropertiesChanged"));

    sig.setArguments(args);
    QDBusConnection::sessionBus().send(sig);
}
