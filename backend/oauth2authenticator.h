#ifndef OAUTH2AUTHENTICATOR_H
#define OAUTH2AUTHENTICATOR_H

#include <QObject>
#include <QUrl>
class QNetworkAccessManager;
class QNetworkReply;
class OAuth2Authenticator : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString token READ token WRITE setToken NOTIFY tokenChanged)
    Q_PROPERTY(QUrl authUrl READ authUrl NOTIFY urlChanged)

public:
    explicit OAuth2Authenticator(QObject *parent = 0);

    QString token() const;
    void setToken(const QString &token);

    QUrl authUrl() const;

signals:
    void tokenChanged();
    void urlChanged();

public slots:
    void onUrlLoad(const QUrl &authUrl);

private slots:
    void onTokenRequestFinished(QNetworkReply *reply);

private:
    QNetworkAccessManager *m_manager;
    QString m_token;
    QUrl m_url;
};

#endif // OAUTH2AUTHENTICATOR_H
