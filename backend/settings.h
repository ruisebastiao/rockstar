#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QSettings>
#include "settings_adaptor.h"

class Settings : public QObject
{
    Q_OBJECT
public:
    Settings(QObject *parent = 0);

    Q_INVOKABLE void setValue(const QString &key, const QDBusVariant &value);
    Q_INVOKABLE QDBusVariant value(const QString &key, const QDBusVariant &defaultValue = QDBusVariant());

private:
    SettingsAdaptor *m_adaptor;
    QSettings *m_settings;
};

#endif // SETTINGS_H
