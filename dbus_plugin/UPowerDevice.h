/*This file is auto generate by pkg.deepin.io/dbus-generator. Don't edit it*/
#include <QtDBus>
QVariant unmarsh(const QVariant&);
QVariant marsh(QDBusArgument target, const QVariant& arg, const QString& sig);
QVariant translateI18n(const char* dir, const char* domain,  const QVariant v);

#ifndef __Device_H__
#define __Device_H__

class DeviceProxyer: public QDBusAbstractInterface
{
    Q_OBJECT
public:
    DeviceProxyer(const QString &path, QObject* parent)
          :QDBusAbstractInterface("pub.geekcentral.Rockstar", path, "org.freedesktop.UPower.Device", QDBusConnection::sessionBus(), parent)
    {
	    if (!isValid()) {
		    qDebug() << "Create Device remote object failed : " << lastError().message();
	    }
    }
    QVariant fetchProperty(const char* name) {
	QDBusMessage msg = QDBusMessage::createMethodCall(service(), path(),
		QLatin1String("org.freedesktop.DBus.Properties"),
		QLatin1String("Get"));
	msg << interface() << QString::fromUtf8(name);
	QDBusMessage reply = connection().call(msg, QDBus::Block, timeout());
	if (reply.type() != QDBusMessage::ReplyMessage) {
	    qDebug () << QDBusError(reply) << "at " << service() << path() << interface() << name;
	    return QVariant();
	}
	if (reply.signature() != QLatin1String("v")) {
	    QString errmsg = QLatin1String("Invalid signature org.freedesktop.DBus.Propertyies in return from call to ");
	    qDebug () << QDBusError(QDBusError::InvalidSignature, errmsg.arg(reply.signature()));
	    return QVariant();
	}

	QVariant value = qvariant_cast<QDBusVariant>(reply.arguments().at(0)).variant();
	return value;
    }


    Q_PROPERTY(QDBusVariant IconName READ __get_IconName__ )
    QDBusVariant __get_IconName__() { return QDBusVariant(fetchProperty("IconName")); }
    

    Q_PROPERTY(QDBusVariant WarningLevel READ __get_WarningLevel__ )
    QDBusVariant __get_WarningLevel__() { return QDBusVariant(fetchProperty("WarningLevel")); }
    

    Q_PROPERTY(QDBusVariant Technology READ __get_Technology__ )
    QDBusVariant __get_Technology__() { return QDBusVariant(fetchProperty("Technology")); }
    

    Q_PROPERTY(QDBusVariant Capacity READ __get_Capacity__ )
    QDBusVariant __get_Capacity__() { return QDBusVariant(fetchProperty("Capacity")); }
    

    Q_PROPERTY(QDBusVariant IsRechargeable READ __get_IsRechargeable__ )
    QDBusVariant __get_IsRechargeable__() { return QDBusVariant(fetchProperty("IsRechargeable")); }
    

    Q_PROPERTY(QDBusVariant State READ __get_State__ )
    QDBusVariant __get_State__() { return QDBusVariant(fetchProperty("State")); }
    

    Q_PROPERTY(QDBusVariant IsPresent READ __get_IsPresent__ )
    QDBusVariant __get_IsPresent__() { return QDBusVariant(fetchProperty("IsPresent")); }
    

    Q_PROPERTY(QDBusVariant Temperature READ __get_Temperature__ )
    QDBusVariant __get_Temperature__() { return QDBusVariant(fetchProperty("Temperature")); }
    

    Q_PROPERTY(QDBusVariant Percentage READ __get_Percentage__ )
    QDBusVariant __get_Percentage__() { return QDBusVariant(fetchProperty("Percentage")); }
    

    Q_PROPERTY(QDBusVariant TimeToFull READ __get_TimeToFull__ )
    QDBusVariant __get_TimeToFull__() { return QDBusVariant(fetchProperty("TimeToFull")); }
    

    Q_PROPERTY(QDBusVariant TimeToEmpty READ __get_TimeToEmpty__ )
    QDBusVariant __get_TimeToEmpty__() { return QDBusVariant(fetchProperty("TimeToEmpty")); }
    

    Q_PROPERTY(QDBusVariant Luminosity READ __get_Luminosity__ )
    QDBusVariant __get_Luminosity__() { return QDBusVariant(fetchProperty("Luminosity")); }
    

    Q_PROPERTY(QDBusVariant Voltage READ __get_Voltage__ )
    QDBusVariant __get_Voltage__() { return QDBusVariant(fetchProperty("Voltage")); }
    

    Q_PROPERTY(QDBusVariant EnergyRate READ __get_EnergyRate__ )
    QDBusVariant __get_EnergyRate__() { return QDBusVariant(fetchProperty("EnergyRate")); }
    

    Q_PROPERTY(QDBusVariant EnergyFullDesign READ __get_EnergyFullDesign__ )
    QDBusVariant __get_EnergyFullDesign__() { return QDBusVariant(fetchProperty("EnergyFullDesign")); }
    

    Q_PROPERTY(QDBusVariant EnergyFull READ __get_EnergyFull__ )
    QDBusVariant __get_EnergyFull__() { return QDBusVariant(fetchProperty("EnergyFull")); }
    

    Q_PROPERTY(QDBusVariant EnergyEmpty READ __get_EnergyEmpty__ )
    QDBusVariant __get_EnergyEmpty__() { return QDBusVariant(fetchProperty("EnergyEmpty")); }
    

    Q_PROPERTY(QDBusVariant Energy READ __get_Energy__ )
    QDBusVariant __get_Energy__() { return QDBusVariant(fetchProperty("Energy")); }
    

    Q_PROPERTY(QDBusVariant Online READ __get_Online__ )
    QDBusVariant __get_Online__() { return QDBusVariant(fetchProperty("Online")); }
    

    Q_PROPERTY(QDBusVariant HasStatistics READ __get_HasStatistics__ )
    QDBusVariant __get_HasStatistics__() { return QDBusVariant(fetchProperty("HasStatistics")); }
    

    Q_PROPERTY(QDBusVariant HasHistory READ __get_HasHistory__ )
    QDBusVariant __get_HasHistory__() { return QDBusVariant(fetchProperty("HasHistory")); }
    

    Q_PROPERTY(QDBusVariant PowerSupply READ __get_PowerSupply__ )
    QDBusVariant __get_PowerSupply__() { return QDBusVariant(fetchProperty("PowerSupply")); }
    

    Q_PROPERTY(QDBusVariant Type READ __get_Type__ )
    QDBusVariant __get_Type__() { return QDBusVariant(fetchProperty("Type")); }
    

    Q_PROPERTY(QDBusVariant UpdateTime READ __get_UpdateTime__ )
    QDBusVariant __get_UpdateTime__() { return QDBusVariant(fetchProperty("UpdateTime")); }
    

    Q_PROPERTY(QDBusVariant Serial READ __get_Serial__ )
    QDBusVariant __get_Serial__() { return QDBusVariant(fetchProperty("Serial")); }
    

    Q_PROPERTY(QDBusVariant Model READ __get_Model__ )
    QDBusVariant __get_Model__() { return QDBusVariant(fetchProperty("Model")); }
    

    Q_PROPERTY(QDBusVariant Vendor READ __get_Vendor__ )
    QDBusVariant __get_Vendor__() { return QDBusVariant(fetchProperty("Vendor")); }
    

    Q_PROPERTY(QDBusVariant NativePath READ __get_NativePath__ )
    QDBusVariant __get_NativePath__() { return QDBusVariant(fetchProperty("NativePath")); }
    


Q_SIGNALS:
};

class Device : public QObject 
{
    Q_OBJECT
private:
    QString m_path;
    Q_SLOT void _propertiesChanged(const QDBusMessage &msg) {
	    QList<QVariant> arguments = msg.arguments();
	    if (3 != arguments.count())
	    	return;
	    QString interfaceName = msg.arguments().at(0).toString();
	    if (interfaceName != "org.freedesktop.UPower.Device")
		    return;

	    QVariantMap changedProps = qdbus_cast<QVariantMap>(arguments.at(1).value<QDBusArgument>());
	    foreach(const QString &prop, changedProps.keys()) {
		    if (0) { 
		    } else if (prop == "IconName") {
			    Q_EMIT __iconNameChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "WarningLevel") {
			    Q_EMIT __warningLevelChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "Technology") {
			    Q_EMIT __technologyChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "Capacity") {
			    Q_EMIT __capacityChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "IsRechargeable") {
			    Q_EMIT __isRechargeableChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "State") {
			    Q_EMIT __stateChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "IsPresent") {
			    Q_EMIT __isPresentChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "Temperature") {
			    Q_EMIT __temperatureChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "Percentage") {
			    Q_EMIT __percentageChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "TimeToFull") {
			    Q_EMIT __timeToFullChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "TimeToEmpty") {
			    Q_EMIT __timeToEmptyChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "Luminosity") {
			    Q_EMIT __luminosityChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "Voltage") {
			    Q_EMIT __voltageChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "EnergyRate") {
			    Q_EMIT __energyRateChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "EnergyFullDesign") {
			    Q_EMIT __energyFullDesignChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "EnergyFull") {
			    Q_EMIT __energyFullChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "EnergyEmpty") {
			    Q_EMIT __energyEmptyChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "Energy") {
			    Q_EMIT __energyChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "Online") {
			    Q_EMIT __onlineChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "HasStatistics") {
			    Q_EMIT __hasStatisticsChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "HasHistory") {
			    Q_EMIT __hasHistoryChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "PowerSupply") {
			    Q_EMIT __powerSupplyChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "Type") {
			    Q_EMIT __typeChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "UpdateTime") {
			    Q_EMIT __updateTimeChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "Serial") {
			    Q_EMIT __serialChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "Model") {
			    Q_EMIT __modelChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "Vendor") {
			    Q_EMIT __vendorChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "NativePath") {
			    Q_EMIT __nativePathChanged__(unmarsh(changedProps.value(prop)));
		    }
	    }
    }
    void _rebuild() 
    { 
	  delete m_ifc;
          m_ifc = new DeviceProxyer(m_path, this);
	  _setupSignalHandle();
    }
    void _setupSignalHandle() {

    }
public:
    Q_PROPERTY(QString path READ path WRITE setPath NOTIFY pathChanged)
    const QString path() {
	    return m_path;
    }
    void setPath(const QString& path) {
	    QDBusConnection::sessionBus().disconnect("pub.geekcentral.Rockstar", m_path, "org.freedesktop.DBus.Properties", "PropertiesChanged",
	    				 this, SLOT(_propertiesChanged(QDBusMessage)));
	    m_path = path;
	    QDBusConnection::sessionBus().connect("pub.geekcentral.Rockstar", m_path, "org.freedesktop.DBus.Properties", "PropertiesChanged",
	    				"sa{sv}as", this, SLOT(_propertiesChanged(QDBusMessage)));
	    _rebuild();
    }
    Q_SIGNAL void pathChanged(QString);

    Device(QObject *parent=0) : QObject(parent), m_ifc(new DeviceProxyer("/org/freedesktop/UPower/Device", this))
    {
	    _setupSignalHandle();
	    QDBusConnection::sessionBus().connect("pub.geekcentral.Rockstar", m_path, "org.freedesktop.DBus.Properties", "PropertiesChanged",
	    				"sa{sv}as", this, SLOT(_propertiesChanged(QDBusMessage)));
    }
    
    Q_PROPERTY(QVariant iconName READ __get_IconName__  NOTIFY __iconNameChanged__)
    Q_PROPERTY(QVariant warningLevel READ __get_WarningLevel__  NOTIFY __warningLevelChanged__)
    Q_PROPERTY(QVariant technology READ __get_Technology__  NOTIFY __technologyChanged__)
    Q_PROPERTY(QVariant capacity READ __get_Capacity__  NOTIFY __capacityChanged__)
    Q_PROPERTY(QVariant isRechargeable READ __get_IsRechargeable__  NOTIFY __isRechargeableChanged__)
    Q_PROPERTY(QVariant state READ __get_State__  NOTIFY __stateChanged__)
    Q_PROPERTY(QVariant isPresent READ __get_IsPresent__  NOTIFY __isPresentChanged__)
    Q_PROPERTY(QVariant temperature READ __get_Temperature__  NOTIFY __temperatureChanged__)
    Q_PROPERTY(QVariant percentage READ __get_Percentage__  NOTIFY __percentageChanged__)
    Q_PROPERTY(QVariant timeToFull READ __get_TimeToFull__  NOTIFY __timeToFullChanged__)
    Q_PROPERTY(QVariant timeToEmpty READ __get_TimeToEmpty__  NOTIFY __timeToEmptyChanged__)
    Q_PROPERTY(QVariant luminosity READ __get_Luminosity__  NOTIFY __luminosityChanged__)
    Q_PROPERTY(QVariant voltage READ __get_Voltage__  NOTIFY __voltageChanged__)
    Q_PROPERTY(QVariant energyRate READ __get_EnergyRate__  NOTIFY __energyRateChanged__)
    Q_PROPERTY(QVariant energyFullDesign READ __get_EnergyFullDesign__  NOTIFY __energyFullDesignChanged__)
    Q_PROPERTY(QVariant energyFull READ __get_EnergyFull__  NOTIFY __energyFullChanged__)
    Q_PROPERTY(QVariant energyEmpty READ __get_EnergyEmpty__  NOTIFY __energyEmptyChanged__)
    Q_PROPERTY(QVariant energy READ __get_Energy__  NOTIFY __energyChanged__)
    Q_PROPERTY(QVariant online READ __get_Online__  NOTIFY __onlineChanged__)
    Q_PROPERTY(QVariant hasStatistics READ __get_HasStatistics__  NOTIFY __hasStatisticsChanged__)
    Q_PROPERTY(QVariant hasHistory READ __get_HasHistory__  NOTIFY __hasHistoryChanged__)
    Q_PROPERTY(QVariant powerSupply READ __get_PowerSupply__  NOTIFY __powerSupplyChanged__)
    Q_PROPERTY(QVariant type READ __get_Type__  NOTIFY __typeChanged__)
    Q_PROPERTY(QVariant updateTime READ __get_UpdateTime__  NOTIFY __updateTimeChanged__)
    Q_PROPERTY(QVariant serial READ __get_Serial__  NOTIFY __serialChanged__)
    Q_PROPERTY(QVariant model READ __get_Model__  NOTIFY __modelChanged__)
    Q_PROPERTY(QVariant vendor READ __get_Vendor__  NOTIFY __vendorChanged__)
    Q_PROPERTY(QVariant nativePath READ __get_NativePath__  NOTIFY __nativePathChanged__)

    //Property read methods
    const QVariant __get_IconName__() { return unmarsh(m_ifc->__get_IconName__().variant()); }
    const QVariant __get_WarningLevel__() { return unmarsh(m_ifc->__get_WarningLevel__().variant()); }
    const QVariant __get_Technology__() { return unmarsh(m_ifc->__get_Technology__().variant()); }
    const QVariant __get_Capacity__() { return unmarsh(m_ifc->__get_Capacity__().variant()); }
    const QVariant __get_IsRechargeable__() { return unmarsh(m_ifc->__get_IsRechargeable__().variant()); }
    const QVariant __get_State__() { return unmarsh(m_ifc->__get_State__().variant()); }
    const QVariant __get_IsPresent__() { return unmarsh(m_ifc->__get_IsPresent__().variant()); }
    const QVariant __get_Temperature__() { return unmarsh(m_ifc->__get_Temperature__().variant()); }
    const QVariant __get_Percentage__() { return unmarsh(m_ifc->__get_Percentage__().variant()); }
    const QVariant __get_TimeToFull__() { return unmarsh(m_ifc->__get_TimeToFull__().variant()); }
    const QVariant __get_TimeToEmpty__() { return unmarsh(m_ifc->__get_TimeToEmpty__().variant()); }
    const QVariant __get_Luminosity__() { return unmarsh(m_ifc->__get_Luminosity__().variant()); }
    const QVariant __get_Voltage__() { return unmarsh(m_ifc->__get_Voltage__().variant()); }
    const QVariant __get_EnergyRate__() { return unmarsh(m_ifc->__get_EnergyRate__().variant()); }
    const QVariant __get_EnergyFullDesign__() { return unmarsh(m_ifc->__get_EnergyFullDesign__().variant()); }
    const QVariant __get_EnergyFull__() { return unmarsh(m_ifc->__get_EnergyFull__().variant()); }
    const QVariant __get_EnergyEmpty__() { return unmarsh(m_ifc->__get_EnergyEmpty__().variant()); }
    const QVariant __get_Energy__() { return unmarsh(m_ifc->__get_Energy__().variant()); }
    const QVariant __get_Online__() { return unmarsh(m_ifc->__get_Online__().variant()); }
    const QVariant __get_HasStatistics__() { return unmarsh(m_ifc->__get_HasStatistics__().variant()); }
    const QVariant __get_HasHistory__() { return unmarsh(m_ifc->__get_HasHistory__().variant()); }
    const QVariant __get_PowerSupply__() { return unmarsh(m_ifc->__get_PowerSupply__().variant()); }
    const QVariant __get_Type__() { return unmarsh(m_ifc->__get_Type__().variant()); }
    const QVariant __get_UpdateTime__() { return unmarsh(m_ifc->__get_UpdateTime__().variant()); }
    const QVariant __get_Serial__() { return unmarsh(m_ifc->__get_Serial__().variant()); }
    const QVariant __get_Model__() { return unmarsh(m_ifc->__get_Model__().variant()); }
    const QVariant __get_Vendor__() { return unmarsh(m_ifc->__get_Vendor__().variant()); }
    const QVariant __get_NativePath__() { return unmarsh(m_ifc->__get_NativePath__().variant()); }
    //Property set methods :TODO check access

public Q_SLOTS:  
    QVariant GetStatistics(const QVariant &type_) {
	    QList<QVariant> argumentList;
	    argumentList << marsh(QDBusArgument(), type_, "s");

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("GetStatistics"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at org.freedesktop.UPower.Device.GetStatistics:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    if (args.size() != 1) {
		    qDebug() << "Warning: \"org.freedesktop.UPower.Device.GetStatistics\" excepted one output parameter, but got " << args.size();
		    return QVariant();
	    }
	    return unmarsh(args[0]);
	    
    }
  
    QVariant GetHistory(const QVariant &type_, const QVariant &timespan, const QVariant &resolution) {
	    QList<QVariant> argumentList;
	    argumentList << marsh(QDBusArgument(), type_, "s") << marsh(QDBusArgument(), timespan, "u") << marsh(QDBusArgument(), resolution, "u");

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("GetHistory"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at org.freedesktop.UPower.Device.GetHistory:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    if (args.size() != 1) {
		    qDebug() << "Warning: \"org.freedesktop.UPower.Device.GetHistory\" excepted one output parameter, but got " << args.size();
		    return QVariant();
	    }
	    return unmarsh(args[0]);
	    
    }
  
    QVariant Refresh() {
	    QList<QVariant> argumentList;
	    argumentList;

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("Refresh"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at org.freedesktop.UPower.Device.Refresh:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    return QVariant();
	    
    }


Q_SIGNALS:
//Property changed notify signal
    void __iconNameChanged__(QVariant);
    void __warningLevelChanged__(QVariant);
    void __technologyChanged__(QVariant);
    void __capacityChanged__(QVariant);
    void __isRechargeableChanged__(QVariant);
    void __stateChanged__(QVariant);
    void __isPresentChanged__(QVariant);
    void __temperatureChanged__(QVariant);
    void __percentageChanged__(QVariant);
    void __timeToFullChanged__(QVariant);
    void __timeToEmptyChanged__(QVariant);
    void __luminosityChanged__(QVariant);
    void __voltageChanged__(QVariant);
    void __energyRateChanged__(QVariant);
    void __energyFullDesignChanged__(QVariant);
    void __energyFullChanged__(QVariant);
    void __energyEmptyChanged__(QVariant);
    void __energyChanged__(QVariant);
    void __onlineChanged__(QVariant);
    void __hasStatisticsChanged__(QVariant);
    void __hasHistoryChanged__(QVariant);
    void __powerSupplyChanged__(QVariant);
    void __typeChanged__(QVariant);
    void __updateTimeChanged__(QVariant);
    void __serialChanged__(QVariant);
    void __modelChanged__(QVariant);
    void __vendorChanged__(QVariant);
    void __nativePathChanged__(QVariant);

//DBus Interface's signal
private:
    DeviceProxyer *m_ifc;
};

#endif
