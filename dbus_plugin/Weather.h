/*This file is auto generate by pkg.deepin.io/dbus-generator. Don't edit it*/
#include <QtDBus>
QVariant unmarsh(const QVariant&);
QVariant marsh(QDBusArgument target, const QVariant& arg, const QString& sig);
QVariant translateI18n(const char* dir, const char* domain,  const QVariant v);

#ifndef __Weather_H__
#define __Weather_H__

class WeatherProxyer: public QDBusAbstractInterface
{
    Q_OBJECT
public:
    WeatherProxyer(const QString &path, QObject* parent)
          :QDBusAbstractInterface("pub.geekcentral.Rockstar", path, "pub.geekcentral.Rockstar.Weather", QDBusConnection::sessionBus(), parent)
    {
	    if (!isValid()) {
		    qDebug() << "Create Weather remote object failed : " << lastError().message();
	    }
    }
    QVariant fetchProperty(const char* name) {
	QDBusMessage msg = QDBusMessage::createMethodCall(service(), path(),
		QLatin1String("org.freedesktop.DBus.Properties"),
		QLatin1String("Get"));
	msg << interface() << QString::fromUtf8(name);
	QDBusMessage reply = connection().call(msg, QDBus::Block, timeout());
	if (reply.type() != QDBusMessage::ReplyMessage) {
	    qDebug () << QDBusError(reply) << "at " << service() << path() << interface() << name;
	    return QVariant();
	}
	if (reply.signature() != QLatin1String("v")) {
	    QString errmsg = QLatin1String("Invalid signature org.freedesktop.DBus.Propertyies in return from call to ");
	    qDebug () << QDBusError(QDBusError::InvalidSignature, errmsg.arg(reply.signature()));
	    return QVariant();
	}

	QVariant value = qvariant_cast<QDBusVariant>(reply.arguments().at(0)).variant();
	return value;
    }


    Q_PROPERTY(QDBusVariant iconSource READ __get_iconSource__ )
    QDBusVariant __get_iconSource__() { return QDBusVariant(fetchProperty("iconSource")); }
    

    Q_PROPERTY(QDBusVariant name READ __get_name__ )
    QDBusVariant __get_name__() { return QDBusVariant(fetchProperty("name")); }
    

    Q_PROPERTY(QDBusVariant text READ __get_text__ )
    QDBusVariant __get_text__() { return QDBusVariant(fetchProperty("text")); }
    

    Q_PROPERTY(QDBusVariant temp READ __get_temp__ )
    QDBusVariant __get_temp__() { return QDBusVariant(fetchProperty("temp")); }
    

    Q_PROPERTY(QDBusVariant pressure READ __get_pressure__ )
    QDBusVariant __get_pressure__() { return QDBusVariant(fetchProperty("pressure")); }
    

    Q_PROPERTY(QDBusVariant humidity READ __get_humidity__ )
    QDBusVariant __get_humidity__() { return QDBusVariant(fetchProperty("humidity")); }
    

    Q_PROPERTY(QDBusVariant location READ __get_location__ WRITE __set_location__)
    QDBusVariant __get_location__() { return QDBusVariant(fetchProperty("location")); }
    void __set_location__(const QDBusVariant &v) { setProperty("location", QVariant::fromValue(v)); }

    Q_PROPERTY(QDBusVariant zipcode READ __get_zipcode__ WRITE __set_zipcode__)
    QDBusVariant __get_zipcode__() { return QDBusVariant(fetchProperty("zipcode")); }
    void __set_zipcode__(const QDBusVariant &v) { setProperty("zipcode", QVariant::fromValue(v)); }

    Q_PROPERTY(QDBusVariant metric READ __get_metric__ WRITE __set_metric__)
    QDBusVariant __get_metric__() { return QDBusVariant(fetchProperty("metric")); }
    void __set_metric__(const QDBusVariant &v) { setProperty("metric", QVariant::fromValue(v)); }

    Q_PROPERTY(QDBusVariant windSpeed READ __get_windSpeed__ )
    QDBusVariant __get_windSpeed__() { return QDBusVariant(fetchProperty("windSpeed")); }
    

    Q_PROPERTY(QDBusVariant windDirection READ __get_windDirection__ )
    QDBusVariant __get_windDirection__() { return QDBusVariant(fetchProperty("windDirection")); }
    


Q_SIGNALS:
};

class Weather : public QObject 
{
    Q_OBJECT
private:
    QString m_path;
    Q_SLOT void _propertiesChanged(const QDBusMessage &msg) {
	    QList<QVariant> arguments = msg.arguments();
	    if (3 != arguments.count())
	    	return;
	    QString interfaceName = msg.arguments().at(0).toString();
	    if (interfaceName != "pub.geekcentral.Rockstar.Weather")
		    return;

	    QVariantMap changedProps = qdbus_cast<QVariantMap>(arguments.at(1).value<QDBusArgument>());
	    foreach(const QString &prop, changedProps.keys()) {
		    if (0) { 
		    } else if (prop == "iconSource") {
			    Q_EMIT __iconSourceChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "name") {
			    Q_EMIT __nameChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "text") {
			    Q_EMIT __textChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "temp") {
			    Q_EMIT __tempChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "pressure") {
			    Q_EMIT __pressureChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "humidity") {
			    Q_EMIT __humidityChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "location") {
			    Q_EMIT __locationChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "zipcode") {
			    Q_EMIT __zipcodeChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "metric") {
			    Q_EMIT __metricChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "windSpeed") {
			    Q_EMIT __windSpeedChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "windDirection") {
			    Q_EMIT __windDirectionChanged__(unmarsh(changedProps.value(prop)));
		    }
	    }
    }
    void _rebuild() 
    { 
	  delete m_ifc;
          m_ifc = new WeatherProxyer(m_path, this);
	  _setupSignalHandle();
    }
    void _setupSignalHandle() {

    }
public:
    Q_PROPERTY(QString path READ path WRITE setPath NOTIFY pathChanged)
    const QString path() {
	    return m_path;
    }
    void setPath(const QString& path) {
	    QDBusConnection::sessionBus().disconnect("pub.geekcentral.Rockstar", m_path, "org.freedesktop.DBus.Properties", "PropertiesChanged",
	    				 this, SLOT(_propertiesChanged(QDBusMessage)));
	    m_path = path;
	    QDBusConnection::sessionBus().connect("pub.geekcentral.Rockstar", m_path, "org.freedesktop.DBus.Properties", "PropertiesChanged",
	    				"sa{sv}as", this, SLOT(_propertiesChanged(QDBusMessage)));
	    _rebuild();
    }
    Q_SIGNAL void pathChanged(QString);

    Weather(QObject *parent=0) : QObject(parent), m_ifc(new WeatherProxyer("/pub/geekcentral/Rockstar/Weather", this))
    {
	    _setupSignalHandle();
	    QDBusConnection::sessionBus().connect("pub.geekcentral.Rockstar", m_path, "org.freedesktop.DBus.Properties", "PropertiesChanged",
	    				"sa{sv}as", this, SLOT(_propertiesChanged(QDBusMessage)));
    }
    
    Q_PROPERTY(QVariant iconSource READ __get_iconSource__  NOTIFY __iconSourceChanged__)
    Q_PROPERTY(QVariant name READ __get_name__  NOTIFY __nameChanged__)
    Q_PROPERTY(QVariant text READ __get_text__  NOTIFY __textChanged__)
    Q_PROPERTY(QVariant temp READ __get_temp__  NOTIFY __tempChanged__)
    Q_PROPERTY(QVariant pressure READ __get_pressure__  NOTIFY __pressureChanged__)
    Q_PROPERTY(QVariant humidity READ __get_humidity__  NOTIFY __humidityChanged__)
    Q_PROPERTY(QVariant location READ __get_location__ WRITE __set_location__ NOTIFY __locationChanged__)
    Q_PROPERTY(QVariant zipcode READ __get_zipcode__ WRITE __set_zipcode__ NOTIFY __zipcodeChanged__)
    Q_PROPERTY(QVariant metric READ __get_metric__ WRITE __set_metric__ NOTIFY __metricChanged__)
    Q_PROPERTY(QVariant windSpeed READ __get_windSpeed__  NOTIFY __windSpeedChanged__)
    Q_PROPERTY(QVariant windDirection READ __get_windDirection__  NOTIFY __windDirectionChanged__)

    //Property read methods
    const QVariant __get_iconSource__() { return unmarsh(m_ifc->__get_iconSource__().variant()); }
    const QVariant __get_name__() { return unmarsh(m_ifc->__get_name__().variant()); }
    const QVariant __get_text__() { return unmarsh(m_ifc->__get_text__().variant()); }
    const QVariant __get_temp__() { return unmarsh(m_ifc->__get_temp__().variant()); }
    const QVariant __get_pressure__() { return unmarsh(m_ifc->__get_pressure__().variant()); }
    const QVariant __get_humidity__() { return unmarsh(m_ifc->__get_humidity__().variant()); }
    const QVariant __get_location__() { return unmarsh(m_ifc->__get_location__().variant()); }
    const QVariant __get_zipcode__() { return unmarsh(m_ifc->__get_zipcode__().variant()); }
    const QVariant __get_metric__() { return unmarsh(m_ifc->__get_metric__().variant()); }
    const QVariant __get_windSpeed__() { return unmarsh(m_ifc->__get_windSpeed__().variant()); }
    const QVariant __get_windDirection__() { return unmarsh(m_ifc->__get_windDirection__().variant()); }
    //Property set methods :TODO check access
    void __set_location__(const QVariant &v) {
	    QVariant marshedValue = marsh(QDBusArgument(), v, "i");
	    m_ifc->__set_location__(QDBusVariant(marshedValue));
	    Q_EMIT __locationChanged__(marshedValue);
    }
    void __set_zipcode__(const QVariant &v) {
	    QVariant marshedValue = marsh(QDBusArgument(), v, "s");
	    m_ifc->__set_zipcode__(QDBusVariant(marshedValue));
	    Q_EMIT __zipcodeChanged__(marshedValue);
    }
    void __set_metric__(const QVariant &v) {
	    QVariant marshedValue = marsh(QDBusArgument(), v, "b");
	    m_ifc->__set_metric__(QDBusVariant(marshedValue));
	    Q_EMIT __metricChanged__(marshedValue);
    }

public Q_SLOTS:

Q_SIGNALS:
//Property changed notify signal
    void __iconSourceChanged__(QVariant);
    void __nameChanged__(QVariant);
    void __textChanged__(QVariant);
    void __tempChanged__(QVariant);
    void __pressureChanged__(QVariant);
    void __humidityChanged__(QVariant);
    void __locationChanged__(QVariant);
    void __zipcodeChanged__(QVariant);
    void __metricChanged__(QVariant);
    void __windSpeedChanged__(QVariant);
    void __windDirectionChanged__(QVariant);

//DBus Interface's signal
private:
    WeatherProxyer *m_ifc;
};

#endif
